alignment
angularJS (coffeeScript)
bettercoffeescript
bracketHighlighter
DocBlockr
EditorConfig
Git
GitGutter
Sass
SassBeautify
SidebarEnhancements
SublimeLinter-annotations
SublimeLinter-Coffeelint
SublimeLinter-csslint
SublimeLinter-jshint
SublimeLinter-rubocop
Theme - Soda
Tomorrow color schemes
TrailingSpaces

