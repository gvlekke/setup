#phpHandler() {
#  git clone https://github.com/jubianchi/phpswitch.git
#  sudo phpswitch/bin/installer --global
#  sudo rm -r phpswitch
#}

# install xcode command line tools
xcode-select --install

# install oh my zsh
curl -L https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh

# install homebrew
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# install homebrew cask (for installing apps)
brew install caskroom/cask/brew-cask
export HOMEBREW_CASK_OPTS="--appdir=/Applications/" # set to default application folder
brew tap gapple/services # run brew services
brew tap caskroom/versions

# install java
brew cask install java

# install apps
brew cask install google-chrome
brew cask install android-studio
brew cask install sourcetree
brew cask install virtualbox
brew cask install vagrant
brew cask install android-file-transfer
brew cask install firefox
brew cask install bettertouchtool
brew cask install sequel-pro
brew cask install atom

#brew cask install sublime-text3

#install brews
brew install postgres
brew install mysql
brew install imagemagick
brew install yarn

# ruby
brew install rbenv
brew install rbenv-gem-rehash
brew install ruby-build
rbenv install 2.2.2
rbenv global 2.2.2

gem update --system
gem install bundler rails

# node
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
nvm install node
nvm use node
npm install coffee-script -g
npm install http-server -g

# alias

alias http="st /private/etc/apache2/extra/httpd-vhosts.conf"
alias restarthttp="sudo /usr/sbin/apachectl restart"
alias hosts="sudo nano /etc/hosts"
alias chromeUnsafe='open -a Google\ Chrome --args --disable-web-security'
alias sleep='pmset displaysleepnow'
alias gitRemoveBranches='git branch --merged | grep -v "\*" | grep -v master | grep -v dev | xargs -n 1 git branch -d'
alias restartCam='sudo killall VDCAssistant; sudo killall AppleCameraAssistant'
alias nginit="ng init --style=sass --skip-npm --routing; yarn install"
alias rn-ios="react-native run-ios --simulator \"iPhone 7\""

export PATH=$PATH:/Users/gvlekke/Library/Android/sdk/platform-tools/

# install virtual machine
curl -s https://raw.githubusercontent.com/xdissent/ievms/master/ievms.sh | env IEVMS_VERSIONS="9 10 11" bash
